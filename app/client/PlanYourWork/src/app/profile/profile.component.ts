import { BoardService } from './../services/board.service';
import { MessageService } from './../services/message.service';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import { ProfileService} from '../services/profile.service';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EventEmitterService } from '../services/event-emitter.service';
import { ActivatedRoute } from '@angular/router';
import { Event as NavigationEvent } from "@angular/router";
import { filter } from "rxjs/operators";
import { NavigationStart } from "@angular/router";
import { Router} from '@angular/router';
import { AuthService } from '../services/auth.service';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public name : String;
  public username : String;
  public email : String;
  public currentProfilePic : String = environment.fileDownloadUrl + "default-user.png";
  public currentUsername:String;
  closeModal: string;
  public modalForm: FormGroup;
  public modalFormPassword: FormGroup;
  errorInvalidFormat :boolean = false;
  private activeSubscriptions : Array<Subscription> = [];
  private messages = [];
  public imgToUpload : File;
  sameProfile :boolean;
  passDontMatch: boolean=false;
  fieldTextType: boolean;
  activeModal:NgbModalRef;

  constructor(private formBuilder:FormBuilder,
              private modalService: NgbModal,
              private profileService: ProfileService,
              private authService: AuthService,
              private messageService: MessageService,
              private eventEmitterService: EventEmitterService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private boardService: BoardService,
              private toastrService: ToastrService) {

   this.switchBackAndForth();
   this.switchBetweenProfiles();
   const params = this.activatedRoute.paramMap.pipe(switchMap(params => {
         this.currentUsername = params.get('username');
         return this.profileService.getDiffrentUser(this.currentUsername);

         })).subscribe();
  this.activeSubscriptions.push(params);

  this.profileService.displayUser.subscribe((data:any)=>{
      this.email= data.email;
      this.username= data.username;
      this.name= data.fullname;
      this.currentProfilePic = environment.fileDownloadUrl + data.imgUrl;
    })
    this.modalForm = this.formBuilder.group({
      name:['', [Validators.required]],
      description:[''],
      members:['', [this.usernameValidator()]],
      boardName: ['', [Validators.required, Validators.pattern('([a-zA-Z0-9-]+[ -]?[a-zA-Z0-9-]*)+')]]
    });
    this.modalFormPassword = this.formBuilder.group({
      updatePasswordOld:['',[Validators.required,Validators.minLength(6), Validators.pattern('(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*')]],
      updatePasswordNew:['',[Validators.required,Validators.minLength(6),Validators.pattern('(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*')]],
      updatePasswordNewConfirm:['',[Validators.required,Validators.minLength(6),Validators.pattern('(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*')]],
    });

    const sub = this.messageService.getMessage().subscribe((msg) => {
      if(msg){
        this.messages.push(msg);
        this.activeSubscriptions.push(sub);
      }
      else{
        this.messages = [];
      }
    })
  }

  public setProfile(name : String, username : String, email : String, profilePic : String){
    this.name = name;
    this.username = username;
    this.email = email;
    this.currentProfilePic = environment.fileDownloadUrl + profilePic;
  }
  public handleFileInput(event: Event): void {
    const files: FileList = (event.target as HTMLInputElement).files;
    if (!files.length) {
      return;
    }
    this.imgToUpload = files[0];
    const sub = this.profileService.uploadProfilePic(this.username, this.imgToUpload)
                    .subscribe(
                  (data:any)=>{
                  const user = this.authService.getUserFromToken(data.accessToken, data.refreshToken);
                  this.currentProfilePic = environment.fileDownloadUrl +  user.imgUrl;
        });
    this.activeSubscriptions.push(sub);
  }

  private changeName(name : String){ this.name = name;}
  private changeEmail(email : String){ this.email = email;}

  public  checkEmailFormat(email : String) : boolean{
    let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    let res = regexp.test(email.toString());
    return res;
  }

  public usernameValidator(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {

        const value = control.value;
        if (!value) {
            return null;
        }
        const hasMonkey = /@+/.test(value);
        return hasMonkey ? {invalid : true}: null;
    }
  }

  onChangeName(){
    let htmlcomponent = <HTMLInputElement>document.getElementById("userName");
    let newName = htmlcomponent.value;
    if(newName!==null && newName!=="" && newName!==this.authService.sendUserDataIfExists().fullname){
      this.changeName(newName);
      const body = {newFullname:newName};
      const changeNameSub = this.profileService.UpdateUser(this.authService.sendUserDataIfExists().username,body)
                                .subscribe((data:any) => {
                                  const fetchedUser = this.authService.getUserFromToken(data.accessToken);
                                  this.toastrService.success(data.message, 'Notification');
                                })
     this.activeSubscriptions.push(changeNameSub);
    }
    htmlcomponent.value = this.name.toString();
  }

  onChangeEmail(){
    let htmlcomponent = <HTMLInputElement>document.getElementById("userEmail");
    let newEmail = htmlcomponent.value;

    if(newEmail!==null && newEmail!=="" && newEmail!==this.authService.sendUserDataIfExists().email){
      if(this.checkEmailFormat(newEmail)){
      this.errorInvalidFormat = false;
      this.changeEmail(newEmail);
      const body ={newEmail:newEmail};
      const changeEmailSub = this.profileService.UpdateUser(this.authService.sendUserDataIfExists().username,body)
                              .subscribe((data:any) => {
                                const fetchedUser = this.authService.getUserFromToken(data.accessToken);
                                this.toastrService.success(data.message, 'Notification');


                              });
      this.activeSubscriptions.push(changeEmailSub);
      }
      else{
        this. errorInvalidFormat = true;
      }
    }

    htmlcomponent.value = this.email.toString();
  }



  public get updatePasswordNew(){ return this.modalFormPassword.get('updatePasswordNew');}
  public get updatePasswordNewConfirm(){ return this.modalFormPassword.get('updatePasswordNewConfirm');}
  public get updatePasswordOld(){ return this.modalFormPassword.get('updatePasswordOld');}

  public deleteAcc(){

    const deleteAccSub = this.authService.deleteUser()
                              .subscribe((data:any) => {
                                  window.alert("Your account has been deleted!");
                                  this.activeModal.close();
                                  this.authService.removeTokens();
                                  this.router.navigate(['../']);
                              },
                              (error) => {
                                this.toastrService.error(error.error.message, 'Notification');
                               });

    this.activeSubscriptions.push(deleteAccSub);
  }
  triggerModalOpen(content) { this.activeModal =   this.modalService.open(content);}

  public submitFormPassword(){

    if (this.updatePasswordNew.value !== this.updatePasswordNewConfirm.value) {
      this.passDontMatch=true;
      return;
    }

    this.passDontMatch=false;
      const oldPass : String = this.updatePasswordOld.value;
      const newPass : String = this.updatePasswordNew.value;

     const body = {oldPass:oldPass,newPass:newPass};
     const changePassSub = this.profileService.UpdateUser(this.authService.sendUserDataIfExists().username,body)
                              .subscribe((data:any) => {
                                  this.toastrService.success(data.message, 'Notification');
                                  const fetchedUser = this.authService.getUserFromToken(data.accessToken);
                                  this.modalFormPassword.reset();
                                  this.activeModal.close();
                              },
                              (error) => {
                                this.toastrService.error(error.error.message, 'Notification');
                               });
  this.activeSubscriptions.push(changePassSub);
 }
  public submitForm(){

    if(!this.modalForm.valid){
      return;
    }

    const projectName : String = this.modalForm.get('name').value
    const projectDescription : String =this.modalForm.get('description').value
    let projectMembers : Array<String> =[this.username];
    let boardName: string = this.modalForm.get('boardName').value

    if(this.modalForm.get('members').value) {
      let addedMembers : Array<String> = this.modalForm.get('members').value.split(',').map((user : String) => user.trim());
      addedMembers = addedMembers.filter((user : String, index : number) => addedMembers.indexOf(user) === index && user !== this.authService.sendUserDataIfExists().username);
      projectMembers = projectMembers.concat(addedMembers);
    }

    let sub = this.profileService.createProject(projectName,projectMembers,projectDescription, boardName)
      .subscribe(() => {
        this.activeModal.close();
        this.modalForm.reset();
        this.router.navigate(['../../project', this.boardService.currentProjectId, this.boardService.currentBoardId]);
      },
      (error) => {
        this.modalFormPassword.reset();
        this.toastrService.error(error.error.message, 'Error');
    });

    this.activeSubscriptions.push(sub);
  }

  public logout() {
    this.authService.logout()
      .subscribe(() =>   {
          this.router.navigate(['../login']);
          this.authService.removeTokens();
      });
  }

  toggleFieldTextType() { this.fieldTextType = !this.fieldTextType;}

  ngOnInit(): void {

    if (this.eventEmitterService.subsVar===undefined) {
      this.eventEmitterService.subsVar = this.eventEmitterService.
      invokeFirstComponentFunction.subscribe((user:any) => {
        const helpUser = {fullname: user.fullname, username:user.username, email:user.email,imgUrl:user.imgUrl};
        this.profileService.userSubject.next(helpUser);
        this.setProfile(user.fullname,user.username,user.email, user.imgUrl);
      });
    }

  }

  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub:Subscription) => {sub.unsubscribe()});
  }
  private switchBetweenProfiles(){
    this.activatedRoute.url.subscribe(url =>{
      if(this.authService.sendUserDataIfExists().username===url[1].path){ this.sameProfile =  true; }
       else { this.sameProfile = false;}
     });
  }

  private switchBackAndForth(){

    this.router.events.pipe(filter(( event: NavigationEvent ) => {
      return( event instanceof NavigationStart );
        })).subscribe(( event: NavigationStart ) => {
                         if ( event.restoredState ) {
                              var result = event.url.split('/')[2];
                              let seb = this.profileService.getDiffrentUser(result).subscribe();
                              this.activeSubscriptions.push(seb);
                         }});

  }
}
