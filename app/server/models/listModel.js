const mongoose = require('mongoose');

const listSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },    
    tasks: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'cards',
        required: true
    }]
});

const listModel = mongoose.model('list', listSchema);

module.exports = listModel;
